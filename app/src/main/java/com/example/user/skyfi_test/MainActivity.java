package com.example.user.skyfi_test;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class MainActivity extends FragmentActivity {// this is the first screen that consists of 3 fragments.
                                                    // the service also starts as soon as this activity starts

    /**
     * Identifier for the first fragment.
     */
    public static final int FRAGMENT_ONE = 0;

    /**
     * Identifier for the second fragment.
     */
    public static final int FRAGMENT_TWO = 1;
    public static final int FRAGMENT_THR = 2;
    /**
     * Number of total fragments.
     */
    public static final int FRAGMENTS = 3;

    /**
     * The adapter definition of the fragments.
     */
    private FragmentPagerAdapter _fragmentPagerAdapter;

    /**
     * The ViewPager that hosts the section contents.
     */
    private ViewPager _viewPager;
    protected Button b;
Intent inte;
String STRING2;

    /**
     * List of fragments.
     */
    private List<Fragment> _fragments = new ArrayList<Fragment>();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

         b = (Button)findViewById(R.id.start);
        inte = new Intent(MainActivity.this,loginreal.class);
        // Create fragments.
        _fragments.add(FRAGMENT_ONE, new fragment1());
        _fragments.add(FRAGMENT_TWO, new fragment2());
        _fragments.add(FRAGMENT_THR, new fragment3());


        // Setup the fragments, defining the number of fragments, the screens and titles.
        _fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()){
            @Override
            public int getCount() {
                return FRAGMENTS;
            }
            @Override
            public android.support.v4.app.Fragment getItem(final int position) {
                return _fragments.get(position);
            }
            @Override
            public CharSequence getPageTitle(final int position) {
                switch (position) {
                    case FRAGMENT_ONE:
                        return "Title One";
                    case FRAGMENT_TWO:
                        return "Title Two";
                    default:
                        return null;
                }
            }
        };
        _viewPager = (ViewPager) findViewById(R.id.pager);
        _viewPager.setAdapter(_fragmentPagerAdapter);







    }



    public void oncli(View v)
{
    startActivity(inte);

}


}