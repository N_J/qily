package com.example.user.skyfi_test;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    LatLng marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Notifications.notifyUser(this, "Ski-Fi Running", R.drawable.logo1);
        Toast.makeText(this,"Searching for available wifi's!!",Toast.LENGTH_LONG).show();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng Whozhigh = new LatLng(17.32, 74.36);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Whozhigh,10));


        marker = new LatLng(17.38, 74.4);
        mMap.addMarker(new MarkerOptions().position(marker).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).title("KFC"));

        marker = new LatLng(17.4, 74.4);
        mMap.addMarker(new MarkerOptions().position(marker).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).title("Paradise"));

        marker = new LatLng(17.5, 74.3);
        mMap.addMarker(new MarkerOptions().position(marker).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).title("McDonalds"));

        marker = new LatLng(17.35, 74.41);
        mMap.addMarker(new MarkerOptions().position(marker).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).title("MartHigh"));

        marker = new LatLng(17.32, 74.36);
        mMap.addMarker(new MarkerOptions().position(marker).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).title("WhozHigh"));

        marker = new LatLng(17.33, 74.2);
        mMap.addMarker(new MarkerOptions().position(marker).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).title("Nerf"));

        marker = new LatLng(17.6, 74.44);
        mMap.addMarker(new MarkerOptions().position(marker).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).title("BigShop"));

        marker = new LatLng(17.39, 74.47);
        mMap.addMarker(new MarkerOptions().position(marker).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).title("Tikka"));

        marker = new LatLng(17.38, 74.42);
        mMap.addMarker(new MarkerOptions().position(marker).icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).title("BarbecueNation"));

        mMap.setOnMarkerClickListener(this);   // on marker click is defined in this class


    }

    View DialogView;
    RatingBar rating;

    @Override
    public boolean onMarkerClick(Marker marker) { // dialog has to open when marker is clicked


        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom((marker.getPosition()), 15));;
        String title = marker.getTitle();  //setting the title


        LayoutInflater factory = LayoutInflater.from(this);   //creating dilaog
        DialogView = factory.inflate(R.layout.info_window, null);

        final AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setView(DialogView);
        dialog.show();
        TextView tv = (TextView) DialogView.findViewById(R.id.txt);
        tv.setText(title);  //setting title

        ImageView imgg= (ImageView) DialogView.findViewById(R.id.imgView);
        if(title.equals("KFC")){
            imgg.setImageResource(R.drawable.kfc);
        }else if(title.equals("Paradise")){
            imgg.setImageResource(R.drawable.paradise);
        }else if(title.equals("McDonalds")){
            imgg.setImageResource(R.drawable.mcdonalds);
        }else if(title.equals("MartHigh")){
            imgg.setImageResource(R.drawable.marthigh);
        }else if(title.equals("WhozHigh")){
            imgg.setImageResource(R.drawable.whozhigh);
        }else if(title.equals("Nerf")){
            imgg.setImageResource(R.drawable.nerf);
        }else if(title.equals("BigShop")){
            imgg.setImageResource(R.drawable.bigshop);
        }else if(title.equals("Tikka")){
            imgg.setImageResource(R.drawable.tikka);
        }else if(title.equals("BarbecueNation")){
            imgg.setImageResource(R.drawable.barbecue);
        }

        rating = (RatingBar) DialogView.findViewById(R.id.rating_pre);
        rating.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        rating.setFocusable(false);

        rating.setRating(4);

        final LinearLayout lll=(LinearLayout)DialogView.findViewById(R.id.lll);

        Button gotorate=(Button)DialogView.findViewById(R.id.more);
        gotorate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snackbar = Snackbar
                        .make(lll, "Subscribe Vendors to Rate!!", Snackbar.LENGTH_LONG);

                snackbar.show();

            }
        });

        return false;
    }


}
