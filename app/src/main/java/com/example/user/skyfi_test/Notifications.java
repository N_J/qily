package com.example.user.skyfi_test;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;

/**
 * Created by avan on 21-12-2015.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class Notifications extends NotificationListenerService {

    final private static int mId = 1;
    private static String lastNotificationText = "";
    final private static Object notificationLock = new Object();
    static NotificationCompat.Builder notification;
    static NotificationManager mNotificationManager;
    static Notification notifications;

    public static void notifyUser(Context context, String message, int icon_res_id) {
        //synchronized (notificationLock) {
            notification = new NotificationCompat.Builder(context);
            notification.setContentTitle("SkiFi Status");
            notification.setContentText(message);
            notification.setPriority(Notification.PRIORITY_MAX);
            notification.setSmallIcon(icon_res_id);
            notification.setAutoCancel(false);
            notification.setOngoing(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notification.setCategory(Notification.CATEGORY_STATUS);
            }
            mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            // mId allows you to update the notification later on.
        notifications = notification.build();
        notifications.flags |= Notification.FLAG_NO_CLEAR| Notification.FLAG_ONGOING_EVENT  ;
        mNotificationManager.notify(mId, notifications);
            lastNotificationText = message;
        //}
    }

   /* @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);

        mNotificationManager.notify(mId, notifications);


    }*/


    @Override
    public StatusBarNotification[] getActiveNotifications() {
        return super.getActiveNotifications();

    }
}
